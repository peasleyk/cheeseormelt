import logging
import json
import glob
import sys
import os
import time
import threading
from queue import Queue

import praw
from praw.models import MoreComments
import prawcore
import dataset
import sqlite3
from src.db import _dataB
from src.classify import ai

""""
  Future possible todo
    If needed add locks to database access/actually handle threads
"""
class COM():

  def __init__(self,config):
    self.config = config
    logging.getLogger('com')

    # reddit specific
    self.username = self.config["cheeseormelt"]["username"]
    self.password = self.config["cheeseormelt"]["password"]
    self.client_secret = self.config["cheeseormelt"]["client_secret"]
    self.user_agent = self.config["cheeseormelt"]["user_agent"]
    self.client_id = self.config["cheeseormelt"]["client_id"]

    self.reddit = praw.Reddit(client_id=self.client_id,
                                client_secret=self.client_secret,
                                password=self.password,
                                user_agent=self.user_agent,
                                username=self.username)

    self.listenSub = self.config['cheeseormelt']['listen_subreddit']
    self.continuous = self.config['cheeseormelt']['continuous_subreddit']
    self.callWord = self.config['cheeseormelt']['callword']
    self.rateWait = self.config['cheeseormelt']['wait']

    self.ai = ai(self.config, self.reddit)
    self.db = _dataB()

    self.rateQueue = Queue()
    self.rateLock = False

  def __makeComment(self,post):
    """
       After we determine we already haven't posted on a thread,
       Format the results into a body post then post it, as long as the
       subreddit isn't ignored
    """

    logging.info("Creating comment")
    tags, chosen, notchosen = self.classify(post)

    footer = self.generate_footer(post.subreddit.display_name)

    if not chosen:
      logging.info("No post made, Couldn't classify")
      body = "Oh no I couldn't classify this. Try another sandwich!\n\n\n"
    elif tags[chosen]*100 > 50 :
      body="Based on previous submissions to /r/grilledcheese and /r/melts, I am " \
             "{:.2f}% sure that this picture links to {}, and not {}\n\n" \
             "If you want to help train this bot, reply 'Correct' if I was right" \
              " and 'Incorrect' if I was wrong\n\n".format(tags[chosen]*100, chosen, notchosen)
    else:
      body="I don't think you linked me to a grilled cheese or a melt. Try a different picture\n\n"

    try:
      post.reply(body+footer)
    except praw.exceptions.APIException as e:
      if e.error_type == "TOO_OLD":
        logging.error("Post too old to comment on")
      else:
        self.rateLock = True
        logging.error("Rate limit hit, waiting {} minute(s)".format(self.rateWait))
        time.sleep(self.rateWait*60)
        post.reply(body+footer)
        self.rateLock = False

    if hasattr(post,'submission'):
      self.db.addPost(post.submission.id)


  def __makeReply(self,comment):
    """
      Replies to a correct or incorrect comment
    """

    footer = self.generate_footer(comment.subreddit.display_name)
    body = "Thanks for the feedback! I've retrained my model and " \
           "I should be more accurate now.\n\n"
    comment.reply(body+footer)
    logging.info('Sent reply')

  def test(self):
    """
      Tests our reddit credentials and keys by trying to get our username
      if this fails, your config.yml or reddit crdentials may be wrong
    """

    try:
      logging.info('Current user is - {}, credentials correct'.format(self.reddit.user.me()))
    except praw.exceptions.APIException as e:
      logging.error('Problem, %s'%e)

  def getEntriesAndPost(self):
    """
      This method will get the 10 newest posts in a subreddit and make attempt to classify
      the image and post a reply. Pretty spammy, make sure you have permission
    """

    sub = self.reddit.subreddit(self.continuous)
    if self.db.isIgnored(self.continuous):
      logging.info("Posting in %s ignored" %self.continuous)
      return

    for post in sub.new(limit=10):
      logging.info("Found post, checking db")
      if self.db.notPosted(post.id) and not self.db.isIgnored(post.subreddit.display_name):
        self.__makeComment(post)
        self.db.addPost(post.id)
      else:
        logging.info('Already posted in this thread')

  def listen(self):
    """
      This method will have the bot listen for calls to it with
      a bang, such as !cheesetest
      The bot will then try to classify the image and post
      Perferred way of running the bot
    """
    logging.info("Set up comment listener")
    def checkForCall(comment):
      text = comment.body.split()
      if self.callWord in text:
        return True

    sub = self.reddit.subreddit(self.listenSub)

    for top_level_comment in sub.stream.comments():
      if isinstance(top_level_comment, MoreComments):
        continue
      if checkForCall(top_level_comment):
        logging.info("Found valid comment, checking db")
        if self.db.notPosted(top_level_comment.submission.id):
          if not self.db.isIgnored(top_level_comment.submission.subreddit.display_name):
            self.rateQueue.put(top_level_comment)
          else:
            logging.info("Not posting as subreddit {} is blocked".format(top_level_comment.submission.subreddit.display_name))
        else:
          logging.info("Already posted in thread")
      if not self.rateQueue.empty() and not self.rateLock:
        call = self.rateQueue.get()
        commentThread = threading.Thread(target=self.__makeComment,args=(top_level_comment,))
        commentThread.start()

  def classify(self,post):
    """
      Tries to classifies a posts image
      Returns a dictionary of concepts and values
    """

    chosen, notchosen,tags = None, None, None

    if hasattr(post,'url'):
      tags,imageID = self.ai.getTags(post.url)
      pid = post.id
    else:
      tags,imageID = self.ai.getTags(post.submission.url)
      pid = post.submission.id

    if tags and imageID:
      chosen = max(tags,key=tags.get)
      notchosen = min(tags,key=tags.get)
      self.db.addImage(imageID, pid, chosen, notchosen)

    return tags, chosen, notchosen

  def handleMessages(self):
    """
      Checks for messages sent to the bot. This includes
        - Direct messages asking the bot to stop posting in a subreddit
        - Replies to the bots comments providing feddback on the classification

      Intended to be run as a thread with a second bot instance
      as not to miss and comments from the comment stream

      Will this ever happen with this bot? Probably never
    """
    logging.info("Set up inbox listener")
    def read(message):
      logging.info('Message marked read')
      message.mark_read()

    def processMessage(message):
      if message.body == "Ignore":
        author = message.author.name
        mod = message.author.is_mod
        subredditToIgnore = message.subject
        try:
          self.reddit.subreddits.search_by_name(subredditToIgnore, exact=True)
        except prawcore.exceptions.NotFound:
          read(message)
          return
        # very slow for some reason
        for moderator in self.reddit.subreddit(subredditToIgnore).moderator():
          if moderator.name == author and mod:
            logging.info('Valid message received to ignore %s, Adding to database'%subredditToIgnore)
            self.db.addIgnored(subredditToIgnore)

    def processComment(comment):
      # Note, this is a message object from praw
      accepted = ["correct", "incorrect"]
      if comment.body.lower() in accepted:
        logging.info('Recognition feedback received')
        post = comment.submission.id
        if self.db.isRetrained(post):
          logging.info('Already retrained this image')
        url = comment.submission.url
        imageID, chosen, notChosen = self.db.getIdFromPost(post)
        incorrect = notChosen
        correct = chosen
        if comment.body == 'Incorrect':
          incorrect = chosen
          correct = notChosen

        if imageID and chosen and notChosen:
          self.ai.sendFeedback(imageID, url, correct, incorrect)
          self.ai.uploadPost(url, correct, incorrect)
          self.__makeReply(message)

        self.db.addRetrained(post)

    while True:
      time.sleep(30)
      messages = list(self.reddit.inbox.unread())
      if messages:
        logging.info('Messages received')
        for message in messages:
          logging.info('Processing message from {}'.format(message.author))
          if message.was_comment:
            processComment(message)
          else:
            processMessage(message)
          read(message)

  def configureModels(self):
    self.ai.createModels()

  # Database methods
  def cleanDB(self):
    self.db.cleanDB()

  def displayData(self):
    self.db.displayDB()

  def addIgnored(self,subreddit):
    self.db.addIgnored(subreddit)


  # Adapted From wikitext bot
  def generate_footer(self,subreddit):
    """
      Creates a footer to our message with helpful links for users
    """

    ignore = 'https://www.reddit.com/message/compose?to=NoMeltsAllowed&message=Ignore&subject={}'.format(subreddit)
    message = 'https://www.reddit.com/message/compose?to={}'.format(self.username)

    footer_links =  [
          ['This is a bot', 'https://www.reddit.com/r/botwatch/'],
          ['Ban from subreddit', ignore],
          ['Send me a message', message]
    ]

    footer = "^[ "

    links = []
    for link in footer_links:
        final_desc = "^" + link[0].replace(" ", " ^")
        final_link = "[" + final_desc + "](" + link[1] + ")"
        links.append(final_link)

    for x in range(0,len(links)):
        footer += links[x]
        if x != len(links)-1:
          footer += " ^| "

    footer += " ^]"
    return footer
