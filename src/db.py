import logging
import dataset
import os
import sqlite3

class _dataB():
  """
    Class for custom database use cases of the COM class
  """

  def __init__(self,):

    logging.getLogger('com')
    self.db = None
    self.postedTo = None
    self.classified = None
    self.ignored = None
    self.reTrained = None

    # We don't need these try's and excepts, but they are handy for debug printing
    if os.path.isfile('database/postids.db'):
      logging.info("Connecting to Database")
    else:
      sqlite3.connect('database/postids.db',check_same_thread=False)
      logging.info("Creating 'postids.db' database")
    self.db = dataset.connect('sqlite:///database/postids.db?check_same_thread=False')

    tables = self.db.tables
    # Image history with postid
    if 'classified' not in tables:
      self.classified = self.db.create_table('classified')
    else:
      self.classified = self.db.load_table('classified')

    # Postid history for no duplicate classifies
    if 'postedTo' not in tables:
      self.postedTo = self.db.create_table('postedTo')
    else:
      self.postedTo = self.db.load_table('postedTo')

    # Subreddit ignore checking
    if 'ignored' not in tables:
      self.ignored = self.db.create_table('ignored')
    else:
      self.ignored = self.db.load_table('ignored')

    if 'retrained' not in tables:
      self.retrained = self.db.create_table('retrained')
    else:
      self.retrained = self.db.load_table('retrained')

  def notPosted(self,post):
    """
      Returns true if we haven't posted
    """

    # Move adding post to database outside
    return not self.postedTo.count(postID=post)

  def addPost(self, post):
    """
      Adds a postid to our postedTo table
    """

    self.postedTo.insert_ignore(dict(postID=post),['postID'])

  def isIgnored(self,Isubreddit):
    """
      Checks if a subreddit is in the ignore table
    """

    return self.ignored.count(subreddit=Isubreddit)

  def addIgnored(self,subreddit):
    """
      Adds a subreddit to the ignore table
    """
    self.ignored.insert_ignore(dict(subreddit=subreddit),['subreddit'])
    logging.info("Added %s to ignore list"%subreddit)

  def addImage(self,imageID,postID,chosen,notChosen):
    """
      Adds an image to the classified table
    """
    logging.info('Added image to db')
    self.classified.insert_ignore(dict(postID=postID,imageID=imageID,chosen=chosen,notchosen=notChosen),['imageID']);

  def addRetrained(self,postID):
    """
      Adds a subreddit ID to the retrained table to mark
      a post we already retrained
    """

    self.retrained.insert_ignore(dict(postID=postID),['postID'])

  def isRetrained(self,postID):
    """
      return true if we have already retrained from feedback
    """
    return self.retrained.count(postID=postID)


  def displayDB(self):
    """
      Handy function to print out all tables and rows
    """
    for table in self.db._tables:
      logging.info("Table: %s" %table)
      allEntries = self.db.get_table(table)
      allEntries = allEntries.all()
      logging.info("Entries: ")
      for entry in list(allEntries):
        logging.info(entry)

  def getIdFromPost(self,post):
    results = self.classified.find(postID=post)
    for x in results:
      imageID = x['imageID']
      chosen = x['chosen']
      notChosen = x['notchosen']
      return imageID, chosen,notChosen

    return None, None, None

  def cleanDB(self):
    """
      Function to drop all tables  from our DB for testing purposes
    """
    logging.info("Deleting Tables")
    self.postedTo.delete()
    self.classified.delete()
    self.ignored.delete()
    return