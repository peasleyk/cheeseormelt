import logging
from clarifai.rest import ClarifaiApp
from clarifai.rest import FeedbackInfo
from clarifai.rest import ApiError

class ai():

    def __init__(self, config, reddit):
      logging.getLogger('com')
      self.app = ClarifaiApp(api_key=config["Clarifai"]['key'])
      self.reddit = reddit

      self.modelName = config["Clarifai"]["name"]
      # If we fail to get the "both" model, then we haven't run the script with -cfg
      try:
        self.model = self.app.models.get(self.modelName)
      except ApiError as e:
        logging.error(e)
        logging.info("Please run 'python main.py -cfg' to set up model and concepts")

    def getTags(self, url):
      """
        Takes a url image, sends it to clarifai then processes the result
        If there is no result, clarifai can't determine based on our model,
        then it just passes

        imageId is returned to add to the database
        tagDict contains the values for melt and gilled cheese
      """

      tagDict = {}
      try:
        rlist = self.classify(url)
      except ApiError:
        return None, None

      if rlist:
        for entry in rlist:
          for entries in entry["outputs"][0]["data"]["concepts"]:
            tagDict[entries["name"]] = entries["value"]
            imageID = rlist[0]['outputs'][0]['input']['id']
          return tagDict,imageID

    def classify(self,url):
      """"
        Uses clarifai's image prediction method to get a taglist
      """
      return [self.model.predict_by_url(url)]


    def sendFeedback(self,imageID,url,correct,incorrect):
      """
        Sends feedback to clarify for a wrong classification
      """
      try:
        self.model.send_concept_feedback(input_id=imageID, url=url,
                        concepts=[correct], not_concepts=[incorrect],
                        feedback_info=FeedbackInfo(event_type='annotation',
                                                   output_id='{output_id}',
                                                   end_user_id='{end_user_id}',
                                                   session_id='{session_id}'))
      except ApiError:
        logging.error('Already sent feedback')
        return
    def uploadPost(self,url,correct,incorrect):
      """
        Uploads an image to Clarifai and retrains the model with it
      """

      try:
        self.app.inputs.create_image_from_url(url, concepts=[correct], not_concepts=[incorrect])
      except ApiError:
        logging.error('Already added this image to be classified')
        return

      logging.info('Retraining model')
      self.model.train()

    def createModels(self):
      """
        These methods all relate to creating our custom model.
        addcheese() - Attempts to take the top 1000 images from /r/grilled cheese
                    and add them to the model

        addMelt() - Same thing as above but for melts

        train() - calls clarifai's training method for our model

        Note that we may not get 1000 images, as some are
            -broke/dead
            -self posts
            -articles
            -links to albums
        """

      def __addCheese():
        logging.info("Finding and adding grilled cheese...")
        sub = self.reddit.subreddit("grilledcheese")
        for submission in sub.top(limit=1000):
          image = submission.url
          if "http://i.imgur.com/" in image:
            try:
              self.app.inputs.create_image_from_url(image, concepts=['a grilled cheese'], not_concepts=['a melt'])
              logging.debug("Image found, adding...")
            except ApiError as e:
              logging.warn(e)
              pass
        return

      def __addMelt():
        logging.info("Finding and adding melts...")
        sub = self.reddit.subreddit("melts")
        for submission in sub.top(limit=1000):
          image = submission.url
          if "http://i.imgur.com/" in image:
            try:
              self.app.inputs.create_image_from_url(image, concepts=['a melt'], not_concepts=['a grilled cheese'])
              logging.debug("Image found, adding...")
            except ApiError as e:
              logging.warn(e)
              pass
        return

      # Trains the model after adding the images
      def __train():
        logging.info("Training model...")
        self.model.train()
        return


      try:
        self.app.models.create(model_id=self.modelName, concepts=["a melt", "a grilled cheese"])
        self.model = self.app.models.get(self.modelName)
      except ApiError:
        # If we we can't create the model, we already have it!
        logging.info("Already ran setup!")
        return
      logging.info("Creating model...")
      __addCheese()
      __addMelt()
      __train()
      logging.info("Done training!")
      return

