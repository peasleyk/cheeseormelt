from src.redditBot import COM
import sys
import os
import time
import yaml
import argparse
import logging
import threading

"""
  This uses cheeseormelt.py
  It will run through reddit posts for a given subreddit, and for every
  new post try to determine if it is a grilled cheese or melt

  before starting, run using the -cfg option to set up and train the models,
  "python main.py -cfg"
"""


def cleanDB(comObject):
  """
    Will drop the tables in the database
  """

  comObject.cleanDB()

def displayDB(comObject):
  """
    Prints out every table and row in the database
  """

  comObject.displayData()

def addIgnore(comObject,subreddit):
  """
    Adds a subreddit to ignore to the database
  """

  comObject.addIgnored(subreddit)

def setup(comObject):
  """
    Needs to be run initially. Creates the model and adds images
    from /r/melts and /r/grilled cheese to the model, then trains it

    You will not be able to classify anything without running this
  """

  comObject.configureModels()

def run(comObject):
  """
    Will check for the 10 newest posts on a subreddit every 30 seconds
    and attempt to classify them.

    Please only run this on your subreddit
  """

  while True:
    comObject.getEntriesAndPost()
    time.sleep(30)

def single(comObject):
  """
    Mostly for testing purposes, this method will classify the 10 newest posts
    on a subreddit. Please use your own subreddit
  """

  comObject.getEntriesAndPost()

def test(comObject):
  """
    Attempts to connect to reddit to test your credentials
    Will print out any errors it encounters
  """

  comObject.test()

def listen(messageBot,callBot):
  """
    The callbot will respond to any calls from the callword defined in
    config.yml and attempt to classify the image. The messageBot will
    manage its inbox to add subreddits to ignore from a moderator.
  """

  messageThread = threading.Thread(target=messageBot.handleMessages)
  callThread = threading.Thread(target=callBot.listen)

  messageThread.start()
  callThread.start()

def testsConfig(config):
  """
    Makes sure config.yml is filled out
  """

  for toplevel in config:
    for sublevel in config[toplevel].keys():
      if not config[toplevel][sublevel]:
        logging.error("Please fill out %s field in config" %sublevel)
        sys.exit()

def main(argv):
  config = yaml.safe_load(open("config.yml"))

  logger = logging.getLogger('com')
  logging.basicConfig(format="%(asctime)s %(levelname)s: %(message)s",
                    level=config["logging"]["level"],
                    datefmt='%m/%d/%Y %I:%M:%S')

  parser = argparse.ArgumentParser()
  parser.add_argument("-s", "--single",
                      help="Runs through the 10 newest posts and quits in a subreddit \
                      defined in config.yaml",
                      action='store_true')
  parser.add_argument("-t", "--test",
                      help="Tests your reddit credentials",
                      action='store_true')
  parser.add_argument("-c", "--continuous",
                      help="Continually checks for new posts every 30 seconds in a subreddit \
                      defined in config.yaml",
                      action='store_true')
  parser.add_argument("-l", "--listen",
                      help="Reponds to callword in config.yml in every subreddit, unless ignored",
                      action='store_true')
  parser.add_argument("-cfg", "--configure",
                      help="Creates the model and trains it with images from /r/grilled cheese and /r/melts",
                      action='store_true')
  parser.add_argument("-cl", "--clean",
                      help="Deletes the databases tables",
                      action='store_true')
  parser.add_argument("-p", "--print",
                      help="Prints the current database",
                      action='store_true')
  parser.add_argument("-a", "--add",
                      help="Adds a subreddit to the ignore database")
  parser.parse_args()
  results = vars(parser.parse_args())

  if not len(sys.argv)-1:
    parser.print_help()
    return
  else:
    testsConfig(config)

  bot = COM(config)

  if results["clean"]:
    cleanDB(bot)

  if results["print"]:
    displayDB(bot)

  if results["add"]:
    addIgnore(bot,results["add"])

  if results['listen']:
    bot2 = COM(config)
    listen(bot, bot2)
  if results["single"]:
    single(bot)
  elif results["test"]:
    test(bot)
  elif results["configure"]:
    setup(bot)
  elif results["continuous"]:
    run(bot)


if __name__ == "__main__":
  try:
    main(sys.argv[1:])
  except KeyboardInterrupt:
    sys.exit()
