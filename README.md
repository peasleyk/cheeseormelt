# Cheese Or Melt
----------------

### What is this
---------------

This was a quick hackathon project that I did and wanted to expand upon
It's a reddit bot that tries to determine if a post links to a picture of a melt or grilled cheese. I then continued to work on it.

### How to setup
---------------

#### Install dependencies
Run
```
pipenv install
pipenc shell
```

#### Edit configuration

Rename config.yml.example to config.yml

For reddit, create a reddit account, create an app in your reddit settings, fill in the config.yml file with the required information

https://github.com/reddit/reddit/wiki/OAuth2

For clarifai, create an account on their website and follow these instructions
https://clarifai-python.readthedocs.io/en/latest/install/
Create an application and use the API key, not their cli tool
The model will be made under this application

#### Create the model
Run 
```
python main.py -cfg
```

To create the clarfai model and train it. This only needs to be done once

### How it works
---------------

First, running with -cfg will create a new model with clarifai, and use images from the top 1000 posts on /r/grilled cheese and /r/melts to train a ml model to try and identify the two.

While it's running, posts will be looked at and tried to be categorized based on what the model returns. No garuntees it will be correct. The bot will post a comment with its best guess.

A user can help the bot by providing feedback by replying to a classification comment with either "Correct" or "Incorrect".

### Running the Bot
-------------------

The best way to run the bot would be with the ```--listen``` flag, as this is the least spammy and will handle messages sent. ```--single``` and ```--continuous``` should really only be used in subreddits you own

```
usage: main.py [-h] [-s] [-t] [-c] [-l] [-cfg] [-cl] [-p] [-a ADD]

optional arguments:
  -h, --help         show this help message and exit
  -s, --single       Runs through the 10 newest posts and quits in a subreddit
                     defined in config.yaml
  -t, --test         Tests your reddit credentials
  -c, --continuous   Continually checks for new posts every 30 seconds in a
                     subreddit defined in config.yaml
  -l, --listen       Reponds to callword in config.yml in every subreddit,
                     unless ignored
  -cfg, --configure  Creates the model and trains it with images from
                     /r/grilled cheese and /r/melts
  -cl, --clean       Deletes the databases tables
  -p, --print        Prints the current database
  -a ADD, --add ADD  Adds a subreddit to the ignore database
```

Future improvements
===================
+ Use a library without using an online service (pytorch, TF inception retraining)
+ Add support to train a model with any two subreddits, possibly multiple

Why
==============
Why not

